<?php
define( 'USERGROUP_ADMINISTRATOR', 1 );
define( 'USERGROUP_CHIEF_ADMINISTRATOR', 2 );

define( 'SETTING_ITEMS_PER_PAGE', 24 );


define( 'FILTERS_DEFAULT', 1 );
define( 'FILTERS_STRING', 2 );
define( 'FILTERS_UINT', 3 );

define( 'PARAMETERS_NOTREQUIRED', 1 );
define( 'PARAMETERS_NOTEMPTY', 2 );
define( 'PARAMETERS_UINT', 4 );
define( 'PARAMETERS_INT', 8 );
define( 'PARAMETERS_NUMBER', 16 );
define( 'PARAMETERS_USERNAME', 32 );
define( 'PARAMETERS_NAME', 64 );
define( 'PARAMETERS_EMAIL', 128 );

class util {
    public static function checkPermission( $group = USERGROUP_ADMINISTRATOR, $user_id = FALSE ) {
        global $user;
        if( !$user ) {
            global $uri;
            throw new PermissionException( 'Login required', str_replace( util::_baseUrl( ), '/', $uri ) );
        }
        if( $group == USERGROUP_ADMINISTRATOR && $user[ 'usergroup' ] < USERGROUP_ADMINISTRATOR ) {
            throw new PermissionException( 'Administrators only !' );
        }
        if( $group == USERGROUP_CHIEF_ADMINISTRATOR && $user[ 'usergroup' ] < USERGROUP_CHIEF_ADMINISTRATOR ) {
            throw new PermissionException( 'Chief Administrators !' );
        }
        if( $user_id && $user[ 'id' ] != $user_id ) {
            throw new PermissionException( 'Not allowed for you !' );
        }
    }
    public static function requiredParameters( $parameters, $required ){
        $missing = array();
    
        foreach( $required as $key ) {
            if( !isset( $parameters[ $key ] ) ) {
                array_push( $missing, $key );
            }
        }
    
        if( !$missing ) {
            return true;
        }
    
        throw new MissingParamentersException( $missing );
    }
    /*public static function Redirect( $uri = FALSE , $extra = '' )
    {
        global $settings;
        $uri = ( $uri ? $uri  : (isset( $_SERVER[ 'HTTP_REFERER' ] ) ? $_SERVER[ 'HTTP_REFERER' ]  : $settings[ 'baseurl'] ) );
        if( ( !strstr( $uri,'?' ) ) && isset( $extra[0] )  && $extra[ 0 ] == '&' ){ 
            $extra[ 0 ] = '?';
        }
        if( ( strstr( $uri,'?' ) ) && isset( $extra[0] )  && $extra[ 0 ] == '?' ){ 
            $extra[ 0 ] = '&';
        }
        header( "Location: ". $uri  . $extra);
        die();
    }*/
    public static function baseUrl( ) {
        global $settings;
        echo $settings[ 'baseurl' ];
    }
    /**
     * Return base url
     */
    public static function _baseUrl( ) {
        global $settings;
        return $settings[ 'baseurl' ];
    }
    /**
     * Prints static base url
     */
    public static function static_baseUrl( ) {
        global $settings;
        echo $settings[ 'static_baseurl' ];
    }
    /**
     * Return static base url
     */
    public static function _static_baseUrl( ) {
        global $settings;
        return $settings[ 'static_baseurl' ];
    }
    /**
     * Prints adminstrator base url
     */
    public static function adminstrator_baseUrl( ) {
        global $settings;
        echo $settings[ 'administrator_baseurl' ];
    }
    /**
     * Return adminstrator base url
     */
    public static function _administrator_baseUrl( ) {
        global $settings;
        return $settings[ 'administrator_baseurl' ];
    }
    /**
     * Create site url
     *
     * @param string $meth Method, default value the current controller
     * @param string $control Controller, default value the current controller
     * @param extra Extra string, default empty
     *
     * @return string Returns the url as string
     */
    public static function siteURL( $control = FALSE, $meth = FALSE, $extra = '' ) {
        global $controller;
        global $method;

        //return 'index.php?controller=' . ( $control == FALSE ? $controller : $control ) . '&method='.$method . $extra;
        global $settings;
        //if( $settings[ 'customrurl' ] ) {
        if( $extra != '' && $extra[ 0 ] == '&' ) {
            if( substr( $extra, 0, 5 ) == '&amp;' ) {
                $extra = '?' . substr( $extra, 5 );
            } else {
                $extra[ 0 ] = '?';
            }
        }
        return $settings[ 'baseurl' ] . (!$control ? $controller : $control) . '/' . (!$meth ? $method : $meth) . '/' . $extra;
        //}
        // return 'index.php?controller=' . ($control == FALSE ? $controller : $control) . '&method=' . ($meth == FALSE ? $method : $meth) . $extra;
    }
    public static function siteURL_short( $controller, $extra = '' ) {
        global $settings;
        if( $extra != '' && $extra[ 0 ] == '&' ) {
            if( substr( $extra, 0, 5 ) == '&amp;' ) {
                $extra = '?' . substr( $extra, 5 );
            } else {
                $extra[ 0 ] = '?';
            }
        }
        return $settings[ 'baseurl' ] . $controller  . '/' . $extra;
    }
    public static function administratorURL( $control = FALSE, $meth = FALSE, $extra = '' ) {
        global $controller;
        global $method;

        //return 'index.php?controller=' . ( $control == FALSE ? $controller : $control ) . '&method='.$method . $extra;
        global $settings;
        //if( $settings[ 'customrurl' ] ) {
        if( $extra != '' && $extra[ 0 ] == '&' ) {
            if( substr( $extra, 0, 5 ) == '&amp;' ) {
                $extra = '?' . substr( $extra, 5 );
            } else {
                $extra[ 0 ] = '?';
            }
        }
        return $settings[ 'administrator_baseurl' ] . (!$control ? $controller : $control) . '/' . (!$meth ? $method : $meth) . '/' . $extra;
        //}
        // return 'index.php?controller=' . ($control == FALSE ? $controller : $control) . '&method=' . ($meth == FALSE ? $method : $meth) . $extra;
    }
    public static function _administratorURL( $control = FALSE, $meth = FALSE, $extra = '' ) {
        global $controller;
        global $method;

        global $settings;
        if( $extra != '' && $extra[ 0 ] == '&' ) {
            if( substr( $extra, 0, 5 ) == '&amp;' ) {
                $extra = '?' . substr( $extra, 5 );
            } else {
                $extra[ 0 ] = '?';
            }
        }
        echo $settings[ 'administrator_baseurl' ] . (!$control ? $controller : $control) . '/' . (!$meth ? $method : $meth) . '/' . $extra;
    }
    public static function siteDirectoryUrl( $controller, $method, $extra ) {
        if( $extra != '' && $extra[ 0 ] == '&' ) {
            $extra[ 0 ] = '?';
        }
        return $method . '/' . $controller . '/' . $extra;
    }
    public static function staticPageUrl( $page ) {
        return util::_baseUrl( ) . 'static/' . $page;
    }
    public static function siteURL_withid( $controller, $method, $id, $params = '' ) {
        //return _baseUrl( ) . 'answer/view/?id=' . $id . $params;
        return util::_baseUrl( ) . $controller . '/' . $method . '/' . $id . '/' . $params;
    }
     public static function administratorURL_withid( $controller, $method, $id = FALSE, $params = '' ) {
        //return _baseUrl( ) . 'answer/view/?id=' . $id . $params;
        if( !$id ){
            return util::_administrator_baseUrl( ) . $controller . '/' . $method . '/' . $params;
        }
        return util::_administrator_baseUrl( ) . $controller . '/' . $method . '/' . $id . '/' . $params;
    }
    /**
     * Preview content , first $max characters from input
     *
     * @param string $input Text input
     */
    public static function contentPreview( $input, $max = 24 ) {
    
        if( mb_strlen( $input ) > $max ) {
             echo join("", array_slice(preg_split("//u", $input, -1, PREG_SPLIT_NO_EMPTY), 0, $max));
            //echo substr( $input, 0, $max);  //mb_substr($input,0,$max,"UTF-8"); // mb_substr( $input, 0, $max, 'UTF-8' ) . '...'; 
        } else {
            echo $input;
        }
    }
    public static function _contentPreview( $input, $max = 24 ) {    
        if( mb_strlen( $input ) > $max ) {
            return mb_substr( $input, 0, $max, 'UTF-8' ) . '...';
        } else {
            return $input;
        }
    }
    public static function _contentClean( $input, $max = 100 ){
        /*$input = preg_replace('/<\s*img [^\>]*src\s*=\s*([\"|\'])(.*?)[\"|\'][^\']*?>/', ' ', $input);*/
    
        $input = preg_replace('/<\s*img [^\>]*>/Ui', ' ', $input);
//        $input = preg_replace('/<\s*iframe  [^</iframe>]*</iframe>/', ' ', $input);

        //remove flash ??
        if( mb_strlen( $input ) > $max ) {
            return mb_substr( $input, 0, $max, 'UTF-8' ) . '...';
        } else {
            return $input;
        }
    }
    public static function _contentTextOnly( $input ){
        preg_match_all('/<p>([^<]*)<\/p>/Usi', $input, $matches);
        foreach ($matches as $match)
        {
            echo $match[0];
          // do something with $match
        }
    }
    /**
     * Parse input value as boolean
     *
     * @param string $value Input value
     * @return boolean
     */
    public static function isBoolean( $value ) {
        if( $value && strtolower( $value ) != 'false' ) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    public static function beginsWith($string, $search)
    {
        return (substr($string, 0, strlen($search)) === $search);
    }
    public static function date_formatted( $datetime, $format = 'j M Y G:i' ){
        $date = new DateTime( $datetime );
        echo $date->format( $format );
    }
    public static function _date_formatted( $datetime, $format = 'j M Y G:i' ){
        $date = new DateTime( $datetime );
        return $date->format( $format );
    }
    public static function _date_ago( $datetime ) {

        $diff = time( ) - strtotime( $datetime );

        if( $diff <= 5 ) {
            return 'Now';
        } else if( $diff < 60 ) {
            return grammar_date( floor( $diff ), ' second(s) ago' );
        } else if( $diff < 3600 ) {
            return grammar_date( floor( $diff / 60 ), ' minute(s) ago' );
        } else if( $diff < 86400 ) {
            return grammar_date( floor( $diff / 3600 ), ' hour(s) ago' );
        } else if( $diff < 2592000 ) {
            return grammar_date( floor( $diff / 86400 ), ' day(s) ago' );
        } else if( $diff < 31104000 ) {
            return grammar_date( floor( $diff / 2592000 ), ' month(s) ago' );
        } else {
            return grammar_date( floor( $diff / 31104000 ), ' year(s) ago' );
        }
    }
    public static function grammar_date( $val, $sentence ) {
        return $val . ( $val > 1 ? str_replace( '(s)', 's', $sentence ) : str_replace( '(s)', '', $sentence ) );
        /* 
        if( $val > 1 ) {
            return $val . str_replace( '(s)', 's', $sentence );
        } else {
            return $val . str_replace( '(s)', '', $sentence );
        }*/
    }
    /**
     * Prevent redirecting to deleted item, 
     *
     * @methods Array Array with blacklist urls
     * @target String Url to redirect
     * @extra String Extra message
     */
    public static function prevent_deleted_item( $methods , $target, $extra = '' ){
        if( isset( $_SERVER[ 'HTTP_REFERER' ] ) ){
            foreach ($methods as $key => $value) {            
                if( preg_match( ( '/^' . str_replace( '/', '\/', $value ) . '/' ), $_SERVER[ 'HTTP_REFERER' ] ) ) {
                    Redirect( $target, $extra );
                }
            }
        }
    }
    public static function print_user_content( $content ){
        echo htmlentities( $content , ENT_QUOTES, 'UTF-8');
    }
	public static function filter_id( $id ){
		return filter_var( $id, FILTER_VALIDATE_INT, array( 'options' => array( 'min_range' =>  0 ) ) );
	}
    public static function validate_parameters( &$parameters, $rules, $filter_values = FALSE ){
        if( $filter_values ) {
             $filter_mapping = $filter_values;
        }
        $incorrect = array();
        foreach ($rules as $key => $value) {
            if( !is_array( $value ) ){
                if( !isset( $parameters[ $key ] ) && $value != PARAMETERS_NOTREQUIRED ){
                    array_push( $incorrect, $key );
                    continue;
                }
                
                if( $value == PARAMETERS_UINT && !filter_var( $parameters[ $key ], FILTER_VALIDATE_INT, array( 'options' => array( 'min_range' =>  0 ) ) ) ){
                    array_push( $incorrect, $key );
                }else if( $value == PARAMETERS_INT && !filter_var( $parameters[ $key ], FILTER_VALIDATE_INT ) ){
                    array_push( $incorrect, $key );
                }else if( $value == PARAMETERS_NUMBER && !filter_var( $parameters[ $key ], FILTER_VALIDATE_FLOAT ) ){
                    array_push( $incorrect, $key );
                }else if( $value == PARAMETERS_USERNAME && !preg_match('/^[A-Za-z0-9_]{3,20}$/',$parameters[ $key ]) ){
                    array_push( $incorrect, $key );
                }else if( $value == PARAMETERS_EMAIL && !filter_var( $parameters[ $key ], FILTER_VALIDATE_EMAIL ) ){
                    array_push( $incorrect, $key );
                }else if( $value == PARAMETERS_NOTEMPTY && ( !$parameters[ $key ] || empty( $parameters[ $key ] ) ) ){
                    array_push( $incorrect, $key );
                }
                /*if( $filter_values ){
                    $parameters[ $key ] = filter_var( trim( $parameters[ $key ] ) , ( isset( $filter_mapping[ $value ] ) ? $filter_mapping[ $value ] : $filter_mapping[ FILTER_SANITIZE_STRING ] ) );
                }*/
             }else{ //array
                 if( !isset( $parameters[ $key ] ) && $value[ 'type' ] != PARAMETERS_NOTREQUIRED ){
                    array_push( $incorrect, $key );
                    continue;
                }
				if(!isset( $value[ 'validation' ] ) ){
					continue;
				}
				 
                if( $value[ 'validation' ] == PARAMETERS_UINT && !filter_var( $parameters[ $key ], FILTER_VALIDATE_INT, array( 'options' => array( 'min_range' =>  0 ) ) ) ){
                    array_push( $incorrect, $key );
                }else if( $value[ 'validation' ] == PARAMETERS_INT && !filter_var( $parameters[ $key ], FILTER_VALIDATE_INT ) ){
                    array_push( $incorrect, $key );
                }else if( $value[ 'validation' ] == PARAMETERS_NUMBER && !filter_var( $parameters[ $key ], FILTER_VALIDATE_FLOAT ) ){
                    array_push( $incorrect, $key );
                }else if( $value[ 'validation' ] == PARAMETERS_USERNAME && !preg_match('/^[A-Za-z0-9_]{3,20}$/',$parameters[ $key ]) ){
                    array_push( $incorrect, $key );
                }else if( $value[ 'validation' ] == PARAMETERS_EMAIL && !filter_var( $parameters[ $key ], FILTER_VALIDATE_EMAIL ) ){
                    array_push( $incorrect, $key );
                }else if( $value[ 'validation' ] == PARAMETERS_NOTEMPTY && ( !$parameters[ $key ] || empty( $parameters[ $key ] ) ) ){
                    array_push( $incorrect, $key );
                }
                if( $filter_values ){
                    $parameters[ $key ] = filter_var( trim( $parameters[ $key ] ) , ( isset( $value[ 'filter' ] ) ? $value[ 'filter' ] :  FILTER_SANITIZE_STRING ) );
                }
             }
        }
        if( !$incorrect ) {
            return TRUE;
        }
        throw new IncorrectParamentersException( $incorrect );
    }
    public static function isAjaxRequest(){
        if( !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' ){
            return TRUE;
        }
        return FALSE;
    }
    public static function IsHTTPS(){
        return (isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] == 'on')) || (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && ($_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https'));
    }

    /*****
    *@dir - Directory to destroy
    *@virtual[optional]- whether a virtual directory
    */
    private static function destroyDir($dir, $virtual = false)
    {
        $ds = DIRECTORY_SEPARATOR;
        $dir = $virtual ? realpath($dir) : $dir;
        $dir = substr($dir, -1) == $ds ? substr($dir, 0, -1) : $dir;
        if (is_dir($dir) && $handle = opendir($dir))
        {
            while ($file = readdir($handle))
            {
                if ($file == '.' || $file == '..')
                {
                    continue;
                }
                elseif (is_dir($dir.$ds.$file))
                {
                    destroyDir($dir.$ds.$file);
                }
                else
                {
                    unlink($dir.$ds.$file);
                }
            }
            closedir($handle);
            rmdir($dir);
            return true;
        }
        else
        {
            return false;
        }
    }
    public static function getRealIpAddr(){
    if (!empty($_SERVER['HTTP_CLIENT_IP'])){
        $ip=$_SERVER['HTTP_CLIENT_IP'];
    }elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {  ///to check ip is pass from proxy 
        $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
    }else  {
        $ip=$_SERVER['REMOTE_ADDR'];
    }
        return $ip;
    }
}
?>