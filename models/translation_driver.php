<?php
class driver_database{
	protected static $pdoLink = false;
    public function __construct( $name, $user, $password, $host = 'localhost' ) {
        $options = array();
        $options[ PDO::MYSQL_ATTR_USE_BUFFERED_QUERY ] = true;
        
        if ( !$this::$pdoLink = new PDO( "mysql:dbname={$name};host={$host};charset=utf8", $user, $password, $options ) ) {
            throw new Exception( 'ERROR_DATABASE_CONNECT' );
        }
        $this::$pdoLink -> query('SET NAMES utf8');
        $this::$pdoLink -> setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
    }
    public static function close( ) {
    	if( driver_database::$pdoLink ){
			driver_database::$pdoLink = NULL;
		}
    }
    public static function Execute( $query, $parameters = array() ) {
        $statement = driver_database::$pdoLink -> prepare( $query );
        $statement -> execute( $parameters );
        return $statement -> rowCount( );
    }
    public static function ExecuteLastInsertId( $query, $parameters = array() ) {
        $statement = driver_database::$pdoLink -> prepare( $query );
        $statement -> execute( $parameters );
        return driver_database::$pdoLink -> lastInsertId( );
    }
    public static function ExecuteAndFetch( $query, $parameters = array() ) {
        $statement = driver_database::$pdoLink -> prepare( $query );
        $statement -> execute( $parameters );
        $data =  $statement -> fetch( PDO::FETCH_ASSOC );
        $statement -> closeCursor( );
        return $data;
    }
    public static function ExecuteAndFetchAll( $query, $parameters = array() ) {
        $statement = driver_database::$pdoLink -> prepare( $query );
        $statement -> execute( $parameters );
        $data =  $statement -> fetchAll( PDO::FETCH_ASSOC );
        $statement -> closeCursor( );
        return $data;
    }
	public static function ExecuteAndFetchAllArray( $query, $parameters = array() ) {
        $statement = driver_database::$pdoLink -> prepare( $query );
        $statement -> execute( $parameters );
        $data =  $statement -> fetchAll( PDO::FETCH_COLUMN );
        $statement -> closeCursor( );
        return $data;
    }
	public static function fetch_language( $lang, $project_id ){
		$language_query = '( SELECT translated.translation FROM translated WHERE translated.key_id = `key`.`key` AND project_id = \''. $project_id . '\' AND language_id = \''. $lang . '\' ORDER BY translated.datetime DESC LIMIT 1 ) AS translated ';
		$db_strings= driver_database::ExecuteAndFetchAll( 'SELECT `key`.`key` ,' . $language_query . ' FROM `key` WHERE `key`.project_id = ? ', array( $project_id ) );
		$strings = array();
		foreach( $db_strings AS $string ){
			$strings[ $string[ 'key' ] ] = $string[ 'translated' ];
		}
		return $strings;
	}
}
new driver_database( 'translate','translate', '~G$C)<vJq|I(XT=','83.212.99.26');

?>
