<?php
class NotFoundException extends Exception {
  
}

class PermissionException extends Exception {
    private $return;
    public function __construct( $message, $return = FALSE ) {
        parent::__construct( $message, 1001 );
        $this -> return = $return;
    }
    public function getReturn( ) {
        return $this -> return;
    }
}

class MissingParamentersException extends Exception {
    private $parameters;
    public function __construct( $parameters ) {
        parent::__construct( __( 'missing_parameters' ), 1000 );
        $this -> parameters = $parameters;
    }
    public  function getParameters( ) {
        return $this -> parameters;
    }
}
class IncorrectParamentersException extends Exception {
    private $parameters;
    public function __construct( $parameters ) {
        parent::__construct( __( 'incorrect_parameters'), 1000 );
        $this -> parameters = $parameters;
    }
    public  function getParameters( ) {
        return $this -> parameters;
    }
}
?>