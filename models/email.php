<?php
class email {
    public static function sendEmail( $address, $title, $text, $HTML = TRUE, $account = 'default' ) {
        $accounts = array( 'default' => array( 'mail' => 'nohponex@gmail.com', 'name' => 'NohponeX' ) );
        if( !isset( $accounts[ $account ] ) ){
            $account = 'default';
            error_log( 'not_found|newsletter_email|'. $account );
        }
        $headers   = array();
        $headers[] = "MIME-Version: 1.0" . "\r\n";
        if( !$HTML ){
            $headers[] = 'Content-type: text/plain;charset=utf-8' . "\r\n";
        }else{
            $headers[] = 'Content-type: text/html;charset=utf-8' . "\r\n";
        }
        $headers[] = 'From: ' . $accounts[ $account ]['name'] . ' <' . $accounts[ $account ]['mail'] . '>' . "\r\n";
        $headers[] = 'Reply-To: ' . $accounts[ $account ]['name'] . ' <' . $accounts[ $account ]['mail'] . "\r\n";

        mail( $address, $title, $text, implode('', $headers), ('-f' . $accounts[ $account ]['mail'] ) );
    }
}
?>