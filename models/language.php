<?php
/**
 * Return translated string
 */
function __( $key ) {
    global $strings;
    if( $strings && array_key_exists( $key, $strings ) ) {
        return $strings[ $key ];
    }
    return $key;
}
/**
 * Print tranlated string
 */
function ___( $key ) {
    global $strings;
    /*if( !isset( $strings ) ) {
        return;
    }*/
    if( $strings && array_key_exists( $key, $strings ) ) {
        echo $strings[ $key ];
        return;
    }
    echo $key;
}
?>