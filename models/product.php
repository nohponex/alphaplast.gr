<?php
class product{
	/*public static function listing( $language = 'en' ){
		return database::ExecuteAndFetchAll( 'SELECT product.*, product_translation.title, product_translation.description, category_translation.title AS category_title FROM product LEFT JOIN product_translation ON product.id = product_translation.product_id AND product_translation.language = ? LEFT JOIN category_translation ON category_translation.category_id = product.category_id AND category_translation.language = ? ', array( $language, $language ) );
	}*/
	public static function listing_category( $category_id, $language = 'en' ){
		return database::ExecuteAndFetchAll( 'SELECT product.*, product_translation.title, product_translation.description, category_translation.title AS category_title FROM product LEFT JOIN product_translation ON product.id = product_translation.product_id AND product_translation.language = ? LEFT JOIN category_translation ON category_translation.category_id = product.category_id AND category_translation.language = ? WHERE product.category_id  = ? ', array( $language, $language, $category_id ) );
	}
	public static function category_view( $category_id, $language = 'en' ){
		return database::ExecuteAndFetch( 'SELECT category.*, category_translation.title, category_translation.description, category_translation.title FROM category LEFT JOIN category_translation ON category_translation.category_id = category.id AND category_translation.language = ? WHERE category.id = ? LIMIT 1', array( $language, $category_id ) ); 
	}
	public static function category_listing( $language = 'en' ){
		return database::ExecuteAndFetchAll( 'SELECT category.*, category_translation.title, category_translation.description, category_translation.title FROM category LEFT JOIN category_translation ON category_translation.category_id = category.id AND category_translation.language = ?', array( $language ) ); 
	}
}
?>