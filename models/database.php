<?php
class database {
    protected static $pdoLink = false;
    public function __construct( $name, $user, $password, $host = 'localhost' ) {
        $options = array();
        $options[ PDO::MYSQL_ATTR_USE_BUFFERED_QUERY ] = true;
        
        if ( !$this::$pdoLink = new PDO( "mysql:dbname={$name};host={$host};charset=utf8", $user, $password, $options ) ) {
            throw new Exception( 'ERROR_DATABASE_CONNECT' );
        }
        $this::$pdoLink -> query('SET NAMES utf8');
        $this::$pdoLink -> setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
    }
    public static function close( ) {
    	if( database::$pdoLink ){
			database::$pdoLink = NULL;
		}
    }
	public static function require_database(){
		if( !database::$pdoLink ){
			global $settings;
			new database( $settings[ 'db' ][ 'name' ], $settings[ 'db' ][ 'user' ], $settings[ 'db' ][ 'pass' ],$settings[ 'db' ][ 'host' ]);
    		unset( $settings['db'] );
		}
	}
    public static function Execute( $query, $parameters = array() ) {
        $statement = database::$pdoLink -> prepare( $query );
        $statement -> execute( $parameters );
        return $statement -> rowCount( );
    }
    public static function ExecuteLastInsertId( $query, $parameters = array() ) {
        $statement = database::$pdoLink -> prepare( $query );
        $statement -> execute( $parameters );
        return database::$pdoLink -> lastInsertId( );
    }
    public static function ExecuteAndFetch( $query, $parameters = array() ) {
        $statement = database::$pdoLink -> prepare( $query );
        $statement -> execute( $parameters );
        $data =  $statement -> fetch( PDO::FETCH_ASSOC );
        $statement -> closeCursor( );
        return $data;
    }
    public static function ExecuteAndFetchArray( $query, $parameters = array() ) {
        $statement = database::$pdoLink -> prepare( $query );
        $statement -> execute( $parameters );
        $data =  $statement -> fetch( PDO::FETCH_COLUMN );
        $statement -> closeCursor( );
        return $data;
    }
    public static function ExecuteAndFetchAll( $query, $parameters = array() ) {
        $statement = database::$pdoLink -> prepare( $query );
        $statement -> execute( $parameters );
        $data =  $statement -> fetchAll( PDO::FETCH_ASSOC );
        $statement -> closeCursor( );
        return $data;
    }
    public static function ExecuteAndFetchAllArray( $query, $parameters = array() ) {
        $statement = database::$pdoLink -> prepare( $query );
        $statement -> execute( $parameters );
        $data =  $statement -> fetchAll( PDO::FETCH_COLUMN );
        $statement -> closeCursor( );
        return $data;
    }
}
?>
