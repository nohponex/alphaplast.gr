<?php
class productController{
	public static function listing( $params ){
		global $language;
		database::require_database();
		clude( 'models/product.php' );
		
		$categories = product::category_listing( $language );
		
		$category_id;
		if( isset( $params[ 'id' ] ) ){
			$category_id = util::filter_id( $params[ 'id' ] );
		}else{
			$category_id = 	$categories[ 0 ][ 'id' ];	
		}
		$category = product::category_view( $category_id );
		if( !$category ){
			throw new NotFoundException( __( 'category_not_found') );
		}
		 
		SiteView( array( 'products' => product::listing_category( $category_id, $language ), 'categories' => $categories, 'category' => $category ) , 'product' );
		
	}
}
?>