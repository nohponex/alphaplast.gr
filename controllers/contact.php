<?php
class contactController{
	public static function create( $params ){
		if( $_SERVER[ 'REQUEST_METHOD' ] != 'POST' ) {
			//throw new Exception( __('unsupported_method' ) );	
		}
		//util::requiredParameters( $params, array( 'message', 'name', 'email' ) );
		clude( 'models/email.php' );
		//$params[ 'message' ] = filter_var( trim( $params[ 'message' ] ) , FILTER_SANITIZE_STRING  );
		//$params[ 'name' ] = filter_var( trim( $params[ 'name' ] ) , FILTER_SANITIZE_STRING  );
		//$params[ 'email' ] = filter_var( trim( $params[ 'email' ] ) , FILTER_VALIDATE_EMAIL  );
		
		$form = array(
	        'name' => array( 'title' => 'Name',
	                          'required' => TRUE,
	                          'type' => 'TEXT',
	                          'validation' => PARAMETERS_NOTEMPTY,
	                          'filter' => FILTERS_STRING),
	        'email' => array( 'title' => 'Email',
	                        'required' => TRUE,
	                        'type' => 'EMAIL',
	                        'validation' => PARAMETERS_EMAIL,
	                        'filter' => FILTER_VALIDATE_EMAIL ),
	        'message' => array( 'title' => 'Message',
	                           'required' => TRUE,
	                           'type' => 'TEXTAREA',
	                           'validation' => PARAMETERS_NOTEMPTY,
	                           'filter' => FILTERS_STRING) );
		
        util::validate_parameters( $params, $form, TRUE );                   
		$ip_address = util::getRealIpAddr();
		
		$ownwer_email = 'nohponex@gmail.com'; //'alphaplast@gmail.com';
		
		$text = "Message received :\r\nFrom : " . $params[ 'name' ] . ' - ' . $params[ 'email' ] . "\r\nIP Address :\r\n" . $ip_address  . "\r\n\r\n" . $params[ 'message' ];
		
		email::sendEmail( $ownwer_email, 'New contact from alphaplast.gr', $text );
		
		Redirect( FALSE, '?success=' . __( 'message_sent' ) );
	} 
}
?>