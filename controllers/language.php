<?php
class languageController{
    public static function update( $params ){
        util::requiredParameters( $params, array( 'language' ) );
        global $settings, $language;
        if( in_array( $params[ 'language' ], $settings['languages'] ) ) {
            $language =  $params[ 'language' ];
        	setcookie( 'language', $language, ( time( ) + 3600 * 24 * 30 ), '/', FALSE, FALSE, TRUE );
        }
        Redirect( );
    }
}
?>