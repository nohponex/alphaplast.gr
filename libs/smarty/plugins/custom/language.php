<?php
/*
 * Smarty plugin
 * -------------------------------------------------------------
 * Type:     function
 * Name:     language
 * -------------------------------------------------------------
 */
function smarty_function_lang($params, &$smarty)
{
	global $strings;
	
	if( !isset( $params[ 's' ] ) ) {
        return 'error missing parameter s!';
    }
	if( isset( $strings[ $params[ 's' ] ] )){
		return $strings[ $params[ 's' ] ]; 
	}else{
		return 'key not found !';//$params[ 's' ];
	}
}
function smarty_function_langU($params, &$smarty)
{
	global $strings;
	
	if( !isset( $params[ 's' ] ) ) {
        return 'error missing parameter s!';
    }
	if( isset( $strings[ $params[ 's' ] ] )){
		$search  = array("Ά", "Έ", "Ή", "Ί", "Ϊ", "ΐ", "Ό", "Ύ", "Ϋ", "ΰ", "Ώ");
        $replace = array("Α", "Ε", "Η", "Ι", "Ι", "Ι", "Ο", "Υ", "Υ", "Υ", "Ω");
      	$string = mb_strtoupper($strings[ $params[ 's' ] ], 'UTF-8');
		return str_replace( $search, $replace, $string ); 
	}else{
		return 'key not found !';//$params[ 's' ];
	}
}
?>