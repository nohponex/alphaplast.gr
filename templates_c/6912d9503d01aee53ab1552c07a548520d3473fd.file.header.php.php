<?php /* Smarty version Smarty-3.1.14, created on 2014-10-15 20:38:30
         compiled from "viewers\site\common\header.php" */ ?>
<?php /*%%SmartyHeaderCode:19201543eb1163f4f11-51134489%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '6912d9503d01aee53ab1552c07a548520d3473fd' => 
    array (
      0 => 'viewers\\site\\common\\header.php',
      1 => 1399488025,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '19201543eb1163f4f11-51134489',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'static_base_url' => 0,
    'base_url' => 0,
    'controller' => 0,
    'method' => 0,
    'language' => 0,
    'language_set' => 0,
    'language_title' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_543eb1173e6a18_54812103',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_543eb1173e6a18_54812103')) {function content_543eb1173e6a18_54812103($_smarty_tpl) {?><!DOCTYPE HTML>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Alphaplast</title>
        <link rel="shortcut icon" type="image/png" href="favicon.ico" />
        <meta name="description" content="description here">
	    <meta name="description" content="Alphaplast">
	    <meta name="keywords" content="Alphaplast">
        <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['static_base_url']->value;?>
css/reset.css?v=3" type="text/css" media="screen" />
        <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['static_base_url']->value;?>
css/style.css?v=3" type="text/css" media="screen" />
        <link href="<?php echo $_smarty_tpl->tpl_vars['static_base_url']->value;?>
css/lightbox.css" rel="stylesheet" />
        <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['static_base_url']->value;?>
css/media.css" type="text/css" media="screen" />
        <script src="<?php echo $_smarty_tpl->tpl_vars['static_base_url']->value;?>
js/libraries/jquery-1.10.2.min.js"></script>
        <script src="<?php echo $_smarty_tpl->tpl_vars['static_base_url']->value;?>
js/libraries/prefixfree.min.js"></script>
        <script src="<?php echo $_smarty_tpl->tpl_vars['static_base_url']->value;?>
js/libraries/modernizr.js"></script>
        <script src="<?php echo $_smarty_tpl->tpl_vars['static_base_url']->value;?>
js/libraries/respond.min.js"></script>
        <script src="<?php echo $_smarty_tpl->tpl_vars['static_base_url']->value;?>
js/libraries/responsiveslides.min.js"></script>
        <script src="<?php echo $_smarty_tpl->tpl_vars['static_base_url']->value;?>
js/libraries/lightbox-2.6.min.js"></script>
        <!--[if lt IE 9]>
            <style>
                header
                {
                    margin: 0 auto 20px auto;
                }
                #four_columns .img-item figure span.thumb-screen
                {
                    display:none;
                }  
            </style>
        <![endif]-->
	    <script type="text/javascript">
	        var base_url = '<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
';
	        var controller = '<?php echo $_smarty_tpl->tpl_vars['controller']->value;?>
';
	        var method = '<?php echo $_smarty_tpl->tpl_vars['method']->value;?>
';
	        var language = '<?php echo $_smarty_tpl->tpl_vars['language']->value;?>
';
	    </script>
	</head>

	<body <?php if ($_smarty_tpl->tpl_vars['controller']->value=='product'||$_smarty_tpl->tpl_vars['controller']->value=='terms'||$_smarty_tpl->tpl_vars['controller']->value=='economy'){?> class="inside_body" <?php }?>>
		<?php if ($_smarty_tpl->tpl_vars['language']->value=='en'){?>
		<?php $_smarty_tpl->tpl_vars['language_title'] = new Smarty_variable('Ελληνικά', null, 0);?>
		<?php $_smarty_tpl->tpl_vars['language_set'] = new Smarty_variable('gr', null, 0);?>
		<?php }else{ ?>
		<?php $_smarty_tpl->tpl_vars['language_title'] = new Smarty_variable('English', null, 0);?>
		<?php $_smarty_tpl->tpl_vars['language_set'] = new Smarty_variable('en', null, 0);?>
		<?php }?>

        <header>          
            <h1 class="hidden">Alphaplast</h1> 
            <a class="logo" href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
"><img class="hidden" src="" alt="Alphaplast logo">A</a>
            <div class="language">
				<a class="<?php echo $_smarty_tpl->tpl_vars['language_set']->value;?>
" href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
language/update/?language=<?php echo $_smarty_tpl->tpl_vars['language_set']->value;?>
" title="<?php echo $_smarty_tpl->tpl_vars['language_title']->value;?>
"></a>			
	  		</div>
            <span id="menu_icon"></span>
            <nav <?php if ($_smarty_tpl->tpl_vars['controller']->value=='home'){?> id="navigation"<?php }?>>
                <ul>
                    <li class="home <?php if ($_smarty_tpl->tpl_vars['controller']->value=='home'){?>current<?php }?>"><a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
#home"><?php echo smarty_function_langU(array('s'=>'home'),$_smarty_tpl);?>
</a></li>
                    <li class="company"><a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
#company"><?php echo smarty_function_langU(array('s'=>'company'),$_smarty_tpl);?>
</a></li>
                    <li class="products <?php if ($_smarty_tpl->tpl_vars['controller']->value=='product'){?>current<?php }?>"><a href="<?php if ($_smarty_tpl->tpl_vars['controller']->value=='home'){?><?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
#products<?php }else{ ?><?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
products<?php }?>"><?php echo smarty_function_langU(array('s'=>'products'),$_smarty_tpl);?>
</a></li>
                    <li class="projects"><a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
#projects"><?php echo smarty_function_langU(array('s'=>'projects'),$_smarty_tpl);?>
</a></li>
                    <li class="contact"><a href="<?php if ($_smarty_tpl->tpl_vars['controller']->value=='product'){?><?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
products/#contact<?php }else{ ?><?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
#contact<?php }?>"><?php echo smarty_function_langU(array('s'=>'contact'),$_smarty_tpl);?>
</a></li>
               	</ul>
            </nav>      
            <img id="calculator" src="<?php echo $_smarty_tpl->tpl_vars['static_base_url']->value;?>
images/icons/calculator.png" alt="calculator"/>
            <a id="espa" href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
economy/view"><img src="<?php echo $_smarty_tpl->tpl_vars['static_base_url']->value;?>
images/espa.jpg" alt="economy"></a>
        </header>
        <nav id="wrapper"><?php }} ?>