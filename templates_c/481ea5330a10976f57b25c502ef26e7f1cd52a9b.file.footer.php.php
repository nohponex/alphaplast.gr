<?php /* Smarty version Smarty-3.1.14, created on 2014-10-15 20:38:31
         compiled from "viewers\site\common\footer.php" */ ?>
<?php /*%%SmartyHeaderCode:29671543eb1176d26e4-55713486%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '481ea5330a10976f57b25c502ef26e7f1cd52a9b' => 
    array (
      0 => 'viewers\\site\\common\\footer.php',
      1 => 1406492278,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '29671543eb1176d26e4-55713486',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'controller' => 0,
    'base_url' => 0,
    'static_base_url' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_543eb1177f7679_51056752',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_543eb1177f7679_51056752')) {function content_543eb1177f7679_51056752($_smarty_tpl) {?>		</nav> <!--end of wrapper -->
		<?php if ($_smarty_tpl->tpl_vars['controller']->value!='error'){?>
		<footer id="contact">
        	<h2 class="hidden">AlphaPlast</h2>
            <section class="wrapper">
            	<h3 class="hidden">AlphaPlast</h3>
            	<div id="map-canvas" class="map"></div>
            	<div class="contuct_details">
            		<ul>
            			<li><h4><?php echo smarty_function_lang(array('s'=>'exhibition_premises'),$_smarty_tpl);?>
</h4></li>
            			<li><span><?php echo smarty_function_lang(array('s'=>'address'),$_smarty_tpl);?>
:</span><?php echo smarty_function_lang(array('s'=>'exhibition_address'),$_smarty_tpl);?>
</li>
            			<li><span><?php echo smarty_function_lang(array('s'=>'tel_fax'),$_smarty_tpl);?>
:</span>+30 2310 811348</li>
            			<li>
							<span>e-mail:</span>
							<a href="mailto:alphaplast@hotmail.gr" target="_top">alphaplast@hotmail.gr</a>
						</li>
            			<li id="second"><h4><?php echo smarty_function_lang(array('s'=>'factory'),$_smarty_tpl);?>
</h4></li>
            			<li><span><?php echo smarty_function_lang(array('s'=>'address'),$_smarty_tpl);?>
:</span><?php echo smarty_function_lang(array('s'=>'factory_address'),$_smarty_tpl);?>
</li>
            			<li><span><?php echo smarty_function_lang(array('s'=>'tel_fax'),$_smarty_tpl);?>
:</span>+30 2310 811348</li>
            		</ul>
            		<form action="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
/contact/create" method="post" >
					   	<p><?php echo smarty_function_lang(array('s'=>'contact_us'),$_smarty_tpl);?>
..</p>
					   	<label for="name"><?php echo smarty_function_lang(array('s'=>'full_name'),$_smarty_tpl);?>
</label>
					   	<input type="text" id="name" name="name"/>
					   	<label for="email">Email</label>
					   	<input type="email" id="email" name="email"/>
    				   	<textarea name="message" rows="5" placeholder="<?php echo smarty_function_lang(array('s'=>'type_your_text'),$_smarty_tpl);?>
 .."></textarea>
					  	<input class="button" type="submit" value="<?php echo smarty_function_lang(array('s'=>'send'),$_smarty_tpl);?>
"/>
					</form>
            	</div>
            </section>
            <section id="copyright">
                <div class="wrapper">
                    <!--<div class="designers">Designed and created by <a href="http://www.example.com" target="_blank">Example</a></div> !-->
                    <div id="info"></div>
                    <a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
terms" title="terms of use"><?php echo smarty_function_lang(array('s'=>'terms_of_use'),$_smarty_tpl);?>
</a>
                </div>
            </section>
        </footer>
 		
        <script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['static_base_url']->value;?>
js/libraries/map.js"></script>
        <?php }?>
        <div id="koemmerling">
        	<span class="close">&#10006;</span>
        	<object type="application/x-shockwave-flash" data="http://www.koemmerling.gr/media/rentabilitaetsrechner_v2_gr_koemmerling.swf" width="464" height="557" id="sb-player">
        		<param name="bgcolor" value="#000"><param name="allowfullscreen" value="true">
        		<param name="wmode" value="transparent">
        	</object>
        </div>
        <script src="<?php echo $_smarty_tpl->tpl_vars['static_base_url']->value;?>
js/libraries/jquery.scrollTo.js"></script>
		<script src="<?php echo $_smarty_tpl->tpl_vars['static_base_url']->value;?>
js/libraries/jquery.nav.js"></script>
		<script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['static_base_url']->value;?>
js/main.js?v=3"></script>
		<script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['static_base_url']->value;?>
js/libs.js?v=3"></script>
        <script type="text/javascript">
            Main.Init();
        </script>
        <script>
        
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
        
          ga('create', 'UA-50871834-1', 'alphaplast.gr');
          ga('send', 'pageview');
        
        </script>
    </body>
</html><?php }} ?>