<?php
function clude($path) {
    static $included = array();
    if (!isset($included[$path])) {
        $included[$path] = true;
        return
        include $path;
    }
    return true;
}

date_default_timezone_set('Europe/Athens');
//Get parameters
$params = array_merge( $_GET, $_POST );

$administratorMode = FALSE;

try{
    $viewer = 'site';
    
    $settings = require( 'settings.php' );
        
    if( $settings['testing'] ){
        error_reporting(E_ALL);
        ini_set('display_errors', '1');
    }

    ini_set('error_log', $settings[ 'errorlog_path' ]);
	
    $uri = FALSE;
    if( isset( $_SERVER['HTTP_HOST'] ) && isset( $_SERVER['REQUEST_URI'] ) ){
        $uri = 'http'. ( isset ( $_SERVER['HTTPS']) && $_SERVER['HTTPS'] ? 's' : null) .'://'. $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
        if( strpos( $uri, $settings['static_baseurl'] ) !== FALSE && $settings['static_baseurl'] != $settings['baseurl'] ){
            header( "Location: ". str_replace( $settings['static_baseurl'], $settings['baseurl'], $uri) );
            die();
        }  
    }

    if( isset( $settings[ 'maintenance' ] ) && $settings[ 'maintenance' ] && !isset( $params[ 'maintenance' ] ) ){
        View( 'maintenance', FALSE );
        die();
    } 
    
    $controllerWhitelist = array( 'home', 'language', 'product', 'contact', 'economy', 'terms' );
	
    $methodWhitelist = array( 'view' , 'create', 'delete', 'listing', 'update' );
    
    $controller = isset( $params['controller'] ) ? $params[ 'controller' ] : 'home';
    $method = isset( $params[ 'method' ] ) ? $params[ 'method' ] : 'view';
    
	clude( 'models/error.php' );
	clude( 'models/language.php' );
    clude( 'models/util.php' );
	 
    unset( $params[ 'controller' ], $params[ 'method' ] );
    
	$language = 'en';
	if( isset( $_COOKIE['language']) ){
		if( in_array( $_COOKIE['language'], $settings['languages'] )){
                $language = $_COOKIE['language'];
            }
    }else if( isset( $_SERVER['HTTP_ACCEPT_LANGUAGE'] ) ){
        $a = str_replace('el', 'gr', substr( strtolower( $_SERVER['HTTP_ACCEPT_LANGUAGE']), 0, 2) );
        if( in_array($a , $settings['languages'] )){
            $language = $a;
        }
    }
	    
    require( "language/$language.php" );
	
	 if ( !in_array( $controller, $controllerWhitelist)  ){
        throw new NotFoundException( __( 'controller_not_found' ) );
    }else if( !in_array( $method, $methodWhitelist ) ){
        throw new NotFoundException( __( 'method_not_found' ) );
    }
	
	clude( 'models/database.php' );
    
    
    
    

    require( "controllers/$controller.php" );

    if( !is_callable(  "{$controller}Controller::$method" )  ){
        throw new NotFoundException(  __( 'method_not_found' )  );
    }
    
    call_user_func( array( $controller . 'Controller', $method ), $params );
    database::close();
} catch ( NotFoundException $exception ) {

    if ( !headers_sent() ) {
        header( 'HTTP/1.0 404 Not Found' );
    }
    
    ErrorView( array( 'error' => $exception->getMessage() ,'title' => 'Error 404 Not Found') );

} catch ( PermissionException $exception ) {
    if ( !headers_sent() ) {
        header( 'HTTP/1.0 403 Forbidden' );
    }
    ErrorView( array( 'error' => $exception->getMessage() , 'title' => 'Error') );
} catch ( MissingParamentersException $exception ){
    ErrorView( array( 'error' => $exception->getMessage() . ' : '.  implode(', ' ,  $exception->getParameters()) , 'title' => 'Error Missing Paramenters') );
    
} catch ( IncorrectParamentersException $exception ){
    ErrorView( array( 'error' => $exception->getMessage() . ' : '.  implode(', ' ,  $exception->getParameters()) , 'title' => 'Error Incorrect Paramenters') );
} catch ( Exception $exception ) {
    ErrorView( array( 'code' => 407, 'error' => $exception->getMessage() , 'title' => 'Error') );   
}
function ErrorView( $params, $exception = NULL ){
	if ( !headers_sent() ) {
        header( 'HTTP/1.0 400' ); //. ( isset( $params[ 'code' ] ) ? $params[ 'code' ] : '404' ) );
    }
    global $user;
    global $controller;
    global $viewer;
    
    $controller = 'error';

	if( !isset( $params[ 'title' ] ) ){
		$params[ 'title' ] = __( 'error' );
	}
	View( $viewer, $params, 'error' );
}
/**
 * Call site viewer
 * 
 * @param params Parameters array
 * @param extra Page name 
 */
function SiteView( $params, $page = '' ){
    clude( 'viewers/site.php' );
    siteViewer::view( $params, $page );
}

function View( $viewer, $params = array(), $page = '' ){
    clude( "viewers/$viewer.php" );
    call_user_func( array( $viewer . 'Viewer', 'view' ), $params ,$page );
}
function Redirect( $uri = FALSE , $extra = '' ){
    global $settings;
    
    $uri = ( $uri ? $uri  : (isset( $_SERVER[ 'HTTP_REFERER' ] ) ? $_SERVER[ 'HTTP_REFERER' ]  :  util::_baseUrl() ) );
    
	if( ( !strstr( $uri,'?' ) ) && isset( $extra[0] )  && $extra[ 0 ] == '&' ){ 
		$extra[ 0 ] = '?';
	}
	if( ( strstr( $uri,'?' ) ) && isset( $extra[0] )  && $extra[ 0 ] == '?' ){ 
		$extra[ 0 ] = '&';
	}
	if( $extra ){ 
		$extra = urldecode( $extra );
	}
    header( 'Location: '. $uri  . $extra );
    die();
}
?>