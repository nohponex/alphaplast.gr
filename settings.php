<?php
$settings = array(
    'testing' => true,
    'baseurl' => 'http://alphaplast.gr.nohponex.gr/',
    'static_baseurl' => 'http://alphaplast.gr.nohponex.gr/',
    'maintenance' => false,
    'db' => array(
        'host' => 'localhost',
        'user' => 'alphaplast',
        'pass' => 'FK3nmJmK',
        'name' => 'alphaplast'
    ),
    'languages' => array( 'en', 'gr' ),
    'errorlog_path' => '../error_alphaplast.gr_log.txt',
);
if ( $settings[ 'testing' ] ) {
    $localsettings = @include 'localsettings.php';
    if ( is_array( $localsettings ) ) {
        foreach ( $localsettings as $key => $value ) {
            $settings[ $key ] = $value;
        }
    }
}
return $settings;
?>