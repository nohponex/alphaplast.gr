var Main = {
    Init: function () {
    	
    	/*copyright*/
        var copyright=new Date();
        var update=copyright.getFullYear();
        if ($('footer #copyright #info').length){ 
        	$('footer #copyright #info').html('Alphaplast Copyright &copy ' + update + ', All Rights Reserved');
        }
        
        if ($('.error_page').length){ 
        	$('html').addClass('error_container');
        	$('body').addClass('error_container');
        	$('nav#wrapper').addClass('error_container');
        }

		//Main.InitializeMap();
		
		$('header #calculator, .inside_page #inside_calculator').click(function(){
			$('#koemmerling').fadeIn();
		});
		
		$('#koemmerling .close').click(function(){
			$('#koemmerling').fadeOut();
		});
		
		if ($(window).width() < 1050 ){
			$('header').addClass('inside_header');
			
			$('header.inside_header').animate({
			    "marginTop": "0"
			},1000);
		}
		else if ($(window).width() > 1050 ){
			$('header').animate({
			    "marginLeft": "0"
			},1000);
		}
		
		if ($('#navigation').length){ 
			$('#navigation').onePageNav({
			    currentClass: 'current',
			    scrollSpeed: 750,
			    scrollOffset: 30,
			    scrollThreshold: 0.5,
			    changeHash:true,
			    filter: '',
			    easing: 'swing',
			    begin: function() {
			        
			    },
			    end: function() {
			    	
			    },
			    scrollChange: function($currentListItem) {
			    	//console.log($currentListItem.attr("class").split(' ')[0]);
			    	window.location.hash = $currentListItem.attr("class").split(' ')[0];
			    	/*if($(window).width() > 1215){
				        if( ($('nav ul li.current').hasClass('products') == true) || ($('nav ul li.current').hasClass('projects') == true)){
			    			$('nav ul li a').css('color','#333');
				    	}
				    	else{
				    		$('nav ul li a').css('color','#fff');
				    	}
				    }*/
				}
			});
		
			/*$('nav ul li').click(function(){
				if($(window).width() > 1215){
			        if( ($('nav ul li.current').hasClass('products') == true) || ($('nav ul li.current').hasClass('projects') == true)){
		    			$('nav ul li a').css('color','#333');
			    	}
			    	else{
			    		$('nav ul li a').css('color','#fff');
			    	}
			    }
			});*/
		}
    	
        Main.Slider();
        var error = false;
		$('form').bind('submit', function () {
			$( this ).find( "input" ).removeClass( "error" );
			$.each( $( this ).find( 'input[type="email"]' ), function(){
			   if ( !Main.Libs.ValidateEmail( $( this ).val() ) ){
			   		$( this ).addClass( "error" );
			   		error = true;
			   }
			 });
			 $.each( $( this ).find( 'input[type="text"]' ), function(){
			   if ( Main.Libs.CheckIfEmpty( $( this ).val() ) ){
			   		$( this ).addClass( "error" );
			   		error = true;
			   }
			 });
			 $.each($( this ).find( 'textarea' ), function(){
			    if ( Main.Libs.CheckIfEmpty( $( this ).val() ) ){
			   		$( this ).addClass( "error" );
			   		error = true;
			   }
			 });
			 return !error;
        });
        
        $('header #menu_icon').click(function() {
			  $('header nav').slideToggle();
		});
    },
    Slider: function () {
          //$('.rslides').css('height', $(window).height()*0.8);
          var number = 1;
          $('.rslides').responsiveSlides({
			  auto: true,             // Boolean: Animate automatically, true or false
			  speed: 800,            // Integer: Speed of the transition, in milliseconds
			  timeout: 6000,          // Integer: Time between slide transitions, in milliseconds
			  pager: false,           // Boolean: Show pager, true or false
			  nav: false,             // Boolean: Show navigation, true or false
			  random: false,          // Boolean: Randomize the order of the slides, true or false
			  pause: false,           // Boolean: Pause on hover, true or false
			  pauseControls: false,    // Boolean: Pause when hovering controls, true or false
			  prevText: "Previous",   // String: Text for the "previous" button
			  nextText: "Next",       // String: Text for the "next" button
			  maxwidth: "",           // Integer: Max-width of the slideshow, in pixels
			  navContainer: "",       // Selector: Where controls should be appended to, default is after the 'ul'
			  manualControls: "",     // Selector: Declare custom pager navigation
			  namespace: "",   // String: Change the default namespace used
			  before: function(){
			  	$("#slider_content1,#slider_content2,#slider_content3").hide();
			  },   // Function: Before callback
			  after: function(number){
			  	number++;
			  	$("#slider_content" + number).delay(300).fadeIn(1000);
			  } // Function: After callback
			});

	},
	InitializeMap: function() {
		if ($('footer #map-canvas').length){ 
			  var mapOptions = {
			  	center: new google.maps.LatLng(40.606052,22.953121),
				zoom: 14,
				mapTypeId: google.maps.MapTypeId.ROADMAP,
			    panControl: true,
				panControlOptions: {
				  position: google.maps.ControlPosition.TOP_RIGHT
				},
				zoomControl: true,
				zoomControlOptions: {
				  style: google.maps.ZoomControlStyle.LARGE,
				  position: google.maps.ControlPosition.RIGHT_TOP
				}
			  }
			  var map = new google.maps.Map(document.getElementById("map-canvas"),
			       mapOptions);
			  var marker = new google.maps.Marker({
			      position: new google.maps.LatLng(40.606052,22.953121),
			      map: map,
			      title: 'AlphaPlast, Αλουμίνια'
			  });
		}
	}
};

