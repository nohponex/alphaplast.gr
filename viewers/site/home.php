<section class="container" id="home">
	<h2 class="hidden">Alphaplast slider</h2>
	<article id="slider_content1">
		<h3>H εταιρία Alphaplast εγγυάται άριστη ποιότητα</h3>
	</article>
	<article id="slider_content2">
		<h3>Παρέχει υψηλής αισθητικής προιόντα</h3>
	</article>
	<article id="slider_content3">
		<h3>Εξασφαλίζει την ασφάλεια στο χώρο σας</h3>
	</article>
	<div class="rslides">
		<img src="{$static_base_url}images/slider3.jpg" alt="Alphaplast image 1">
		<img src="{$static_base_url}images/slider2.jpg" alt="Alphaplast image 2">
		<img src="{$static_base_url}images/slider1.jpg" alt="Alphaplast image 3">
	</div>
	<span class="down"></span>
</section>
<section class="boxcontent"  id="company">
	<article>
		<h2>{lang s=company_alphaplast}</h2>
		<p>
			Η εταιρία μας σας παρέχει υψηλής ποιότητας κουφώματα PVC, αλουμινίου, πόρτες, εξώπορτες, είδη σκίασης,
			κουνουπιέρες, κάγκελα αλουμινίου, αίθρια και όσα γενικά χρειάζονται για τη θωράκιση και προστασία του προσωπικού
			σας χώρου. Η εταιρεία μας σύμφωνα με το ευρωπαϊκό πρότυπο EN 14351-1 πιστοποιεί την ποιότητα των κουφωμάτων της.
		</p>
		<p>
			Τα πιστοποιητικά μας (CE), το έμπειρο εξειδικευμένο προσωπικό, τα χρόνια καταξιωμένης καριέρας, οι συνεργασίες
			με τα μεγαλύτερα εργοστάσια της Ευρώπης μπορούν να σας εγγυηθούν άφοβα ότι μπορούμε να καλύψουμε κάθε δική σας
			ιδιαίτερη απαίτηση. Αναλαμβάνουμε σε όλη την Ελλάδα την κατασκευή και την συντήρηση οποιασδήποτε κατασκευής
			κουφωμάτων.
		</p>
		<p>
			Πρόσφατα ακολουθώντας τις απαιτήσεις της εποχής θέλοντας να συνεχίσουμε να προσφέρουμε υψηλής ποιότητας
			λύσεις στις πιο ανταγωνιστικές τιμές, δημιουργήσαμε μία νέα σύγχρονη μονάδα παραγωγής στη γειτονική Βουλγαρία.
		</p>
		<p>
			Είμαστε στη διάθεσή σας για να συζητήσουμε από κοντά τις ανάγκες σας και να σας προσφέρουμε ποιότητα, ασφάλεια και υψηλή αισθητική.
		</p>
	</article>
	<span></span>
</section>
<section class="four_columns" id="products">
	<h2>{lang s=products}</h2>
	<section>
		<article class="img-item">
			<figure>
				<a href="{$base_url}" title="Some title"> <img src="{$static_base_url}images/products/4.jpg" alt="Some alt text"/> </a>
				<figcaption>
					<strong> Συνθετικά Κουφώμάτα </strong>
				</figcaption>
			</figure>
		</article>
		<article class="img-item sec">
			<figure>
				<a href="{$base_url}" title="Some title"> <img src="{$static_base_url}images/pro2.jpg" alt="Some alt text"/> </a>
				<figcaption>
					<strong> Εξώπορτες </strong>
				</figcaption>
			</figure>
		</article>
		<article class="img-item">
			<figure>
				<a href="{$base_url}" title="Some title"> <img src="{$static_base_url}images/pro3.jpg" alt="Some alt text"/> </a>
				<figcaption>
					<strong> Κουφώματα Αλουμινίου </strong>
				</figcaption>
			</figure>
		</article>
		<article class="img-item sec">
			<figure>
				<a href="{$base_url}" title="Some title"> <img src="{$static_base_url}images/pro4.jpg" alt="Some alt text"/> </a>
				<figcaption>
					<strong> Είδη σκίασης </strong>
				</figcaption>
			</figure>
		</article>
	</section>
	<p>
		Τα προϊόντα νέας τεχνολογίας της Alphaplast A.E. χαρακτηρίζονται από σύγχρονες και φιλικές προς το περιβάλλον προδιαγραφές.
	</p>
	{* <a href="{$base_url}products" class="button" title="Όλα τα προιόντα">{lang s=all_products}</a> *}
	
	<section class="suppliers">
		<h2>Προμηθευτές</h2>
		<a href="http://www.profine-group.com/ecomaXL/index.php?site=PROFINE_EN_home" title="Profine" target="_blank"> <img src="{$static_base_url}images/profine.gif" alt="profine"/> </a>
		<a href="http://www.koemmerling.gr/" target="_blank" title="Koemmerling"> <img src="{$static_base_url}images/koemmerling.jpg" alt="koemmerling"/></a>
		<a href="http://www.kbe.gr/" title="KBE" target="_blank"> <img src="{$static_base_url}images/kbe.png" alt="kbe"/> </a>
		<a href="http://ftt.roto-frank.com/en/" target="_blank" title="Roto"> <img src="{$static_base_url}images/roto.png" alt="Roto"/></a>
		<a href="http://www.g-u.com/en/home.html" target="_blank" title="G-U"> <img src="{$static_base_url}images/gu.jpg" alt="G-U"/></a>
	</section>
	<br class="clear"/>
</section>
<section class="three_columns" id="projects">
	<section>
		<h2>{lang s=projects}</h2>
		{for $counter=1 to 26}
		<article class="img-item">
			<figure>
				<a href="{$static_base_url}images/projects/{$counter}.JPG" data-lightbox="roadtrip" title="project"> <span class="thumb-screen"></span> <img src="{$static_base_url}images/projects/{$counter}.JPG" alt=""/> </a>
				<figcaption class="hidden">
					<strong> </strong>
				</figcaption>
			</figure>
		</article>
		{/for}
		<span id="moto">{lang s=moto}</span>
		<br class="clear"/>
	</section>
</section>