<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Alphaplast</title>
        <link rel="shortcut icon" type="image/png" href="favicon.ico" />
        <meta name="description" content="description here">
	    <meta name="description" content="Alphaplast">
	    <meta name="keywords" content="Alphaplast">
        <link rel="stylesheet" href="{$static_base_url}css/reset.css?v=3" type="text/css" media="screen" />
        <link rel="stylesheet" href="{$static_base_url}css/style.css?v=3" type="text/css" media="screen" />
        <link href="{$static_base_url}css/lightbox.css" rel="stylesheet" />
        <link rel="stylesheet" href="{$static_base_url}css/media.css" type="text/css" media="screen" />
        <script src="{$static_base_url}js/libraries/jquery-1.10.2.min.js"></script>
        <script src="{$static_base_url}js/libraries/prefixfree.min.js"></script>
        <script src="{$static_base_url}js/libraries/modernizr.js"></script>
        <script src="{$static_base_url}js/libraries/respond.min.js"></script>
        <script src="{$static_base_url}js/libraries/responsiveslides.min.js"></script>
        <script src="{$static_base_url}js/libraries/lightbox-2.6.min.js"></script>
        <!--[if lt IE 9]>
            <style>
                header
                {
                    margin: 0 auto 20px auto;
                }
                #four_columns .img-item figure span.thumb-screen
                {
                    display:none;
                }  
            </style>
        <![endif]-->
	    <script type="text/javascript">
	        var base_url = '{$base_url}';
	        var controller = '{$controller}';
	        var method = '{$method}';
	        var language = '{$language}';
	    </script>
	</head>

	<body {if $controller == 'product' or $controller == 'terms' or $controller == 'economy'} class="inside_body" {/if}>
		{if $language == 'en'}
		{$language_title= 'Ελληνικά'}
		{$language_set = 'gr' }
		{else}
		{$language_title = 'English'}
		{$language_set = 'en' }
		{/if}

        <header>          
            <h1 class="hidden">Alphaplast</h1> 
            <a class="logo" href="{$base_url}"><img src="{$base_url}images/logo.png" alt="Alphaplast logo"></a>
            <div class="language">
				<a class="{$language_set}" href="{$base_url}language/update/?language={$language_set}" title="{$language_title}"></a>			
	  		</div>
            <span id="menu_icon"></span>
            <nav {if $controller == 'home'} id="navigation"{/if}>
                <ul>
                    <li class="home {if $controller == 'home'}current{/if}"><a href="{$base_url}#home">{langU s=home}</a></li>
                    <li class="company"><a href="{$base_url}#company">{langU s=company}</a></li>
                    <li class="products {if $controller == 'product'}current{/if}"><a href="{if $controller == 'home'}{$base_url}#products{else}{$base_url}products{/if}">{langU s=products}</a></li>
                    <li class="projects"><a href="{$base_url}#projects">{langU s=projects}</a></li>
                    <li class="contact"><a href="{if $controller == 'product'}{$base_url}products/#contact{else}{$base_url}#contact{/if}">{langU s=contact}</a></li>
               	</ul>
            </nav>      
            <img id="calculator" src="{$static_base_url}images/icons/calculator.png" alt="calculator"/>
            <a id="espa" href="{$base_url}economy/view"><img src="{$static_base_url}images/espa.jpg" alt="economy"></a>
        </header>
        <nav id="wrapper">