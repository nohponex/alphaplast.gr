		</nav> <!--end of wrapper -->
		{if $controller != 'error'}
		<footer id="contact">
        	<h2 class="hidden">AlphaPlast</h2>
            <section class="wrapper">
            	<h3 class="hidden">AlphaPlast</h3>
            	<div id="map-canvas" class="map">
            		<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3029.0847566726834!2d22.9530846!3d40.6059536!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x14a839278435198b%3A0xb53b7454c77e2437!2zzpvOtc-Jz4bPjM-Bzr_PgiDOks6xz4POuc67zq_Pg8-DzrfPgiDOjM67zrPOsc-CIDExOSwgzpjOtc-Dz4POsc67zr_Ovc6vzrrOtyA1NDYgNDMsIM6VzrvOu86szrTOsQ!5e0!3m2!1sel!2sus!4v1413399519730" width="100%" height="300" frameborder="0" style="border:0"></iframe>
            	</div>
            	<div class="contuct_details">
            		<ul>
            			<li><h4>{lang s=exhibition_premises}</h4></li>
            			<li><span>{lang s=address}:</span>{lang s=exhibition_address}</li>
            			<li><span>{lang s=tel_fax}:</span>+30 2310 811348</li>
            			<li>
							<span>e-mail:</span>
							<a href="mailto:alphaplast@hotmail.gr" target="_top">alphaplast@hotmail.gr</a>
						</li>
            			<li id="second"><h4>{lang s=factory}</h4></li>
            			<li><span>{lang s=address}:</span>{lang s=factory_address}</li>
            			<li><span>{lang s=tel_fax}:</span>+30 2310 811348</li>
            		</ul>
            		<form action="{$base_url}/contact/create" method="post" >
					   	<p>{lang s=contact_us}..</p>
					   	<label for="name">{lang s=full_name}</label>
					   	<input type="text" id="name" name="name"/>
					   	<label for="email">Email</label>
					   	<input type="email" id="email" name="email"/>
    				   	<textarea name="message" rows="5" placeholder="{lang s=type_your_text} .."></textarea>
					  	<input class="button" type="submit" value="{lang s=send}"/>
					</form>
            	</div>
            </section>
            <section id="copyright">
                <div class="wrapper">
                    <!--<div class="designers">Designed and created by <a href="http://www.example.com" target="_blank">Example</a></div> !-->
                    <div id="info"></div>
                    <a href="{$base_url}terms" title="terms of use">{lang s=terms_of_use}</a>
                </div>
            </section>
        </footer>
 		
        <script type="text/javascript" src="{$static_base_url}js/libraries/map.js"></script>
        {/if}
        <div id="koemmerling">
        	<span class="close">&#10006;</span>
        	<object type="application/x-shockwave-flash" data="http://www.koemmerling.gr/media/rentabilitaetsrechner_v2_gr_koemmerling.swf" width="464" height="557" id="sb-player">
        		<param name="bgcolor" value="#000"><param name="allowfullscreen" value="true">
        		<param name="wmode" value="transparent">
        	</object>
        </div>
        <script src="{$static_base_url}js/libraries/jquery.scrollTo.js"></script>
		<script src="{$static_base_url}js/libraries/jquery.nav.js"></script>
		<script type="text/javascript" src="{$static_base_url}js/main.js?v=3"></script>
		<script type="text/javascript" src="{$static_base_url}js/libs.js?v=3"></script>
        <script type="text/javascript">
            Main.Init();
        </script>
        <script>
        {literal}
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
        
          ga('create', 'UA-50871834-1', 'alphaplast.gr');
          ga('send', 'pageview');
        {/literal}
        </script>
    </body>
</html>