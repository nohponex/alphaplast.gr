<section class="product_listing inside_page" >
	<h2>{lang s=company_products}</h2>
	<ul>
		<li class="not_hovered"><h3>{lang s=categories}</h3></li>
		{foreach $categories as $categ}
			{if $category.id == $categ.id}
				<li class="selected">
					<a href="{$base_url}products/{$categ.id}" title="{$categ.title}">{$categ.title}</a>
				</li>
			{else}
				<li>
	 				<a href="{$base_url}products/{$categ.id}" title="{$categ.title}">{$categ.title}</a>
	 			</li>
	 		{/if}
		{/foreach}
		<li class="not_hovered" id="inside_calculator">
			<p>{lang s=calculator_info}</p>
			<img src="{$static_base_url}images/icons/calculator.png" alt="calculator"/>
		</li>
		<li class="not_hovered" id="inside_espa">
			<a href="{$base_url}economy/view"><img src="{$static_base_url}images/espa.jpg" alt="economy"></a>
		</li>
		
	</ul> 
	<ul class="items">
		{$counter = 0}
		{foreach $products as $product}
			{if $counter%2 == 0}
				<li>
			 		<img src="{$static_base_url}{$product.image}" alt="{$product.title}">
			 		{*<p>{$product.title}</p> *}
			 	</li>
			{else}
				<li>
					{*<p>{$product.title}</p> *}
			 		<img src="{$static_base_url}{$product.image}" alt="{$product.title}">
			 	</li>
	 		{/if}
	 		{$counter = $counter + 1}
		{/foreach}
	</ul>
</section> 

