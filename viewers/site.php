<?php
class siteViewer {
    public static function view( $params = array(), $page ) {
    	clude( 'libs/smarty/Smarty.class.php' );
		clude( 'libs/smarty/plugins/custom/language.php' );
		
		$smarty = new Smarty();
		global $controller, $method, $language;
		
		$smarty->assign( 'base_url', util::_baseUrl() );
		$smarty->assign( 'static_base_url', util::_static_baseUrl() );
		$smarty->assign( 'controller', $controller );
		$smarty->assign( 'method', $method );
		$smarty->assign( 'language', $language );
		
		foreach ($params as $key => $value) {
			$smarty->assign( $key, $value );
		}
		//$smarty->assign('firstname', 'Doug');
		
		$smarty->display( 'viewers/site/common/header.php');
		$smarty->display( 'viewers/site/' . $page . '.php');
		$smarty->display( 'viewers/site/common/footer.php');
        //include ('site/common/header.php');
        //include ('site/' . $page . '.php');
        //include ('site/common/footer.php');
    }
}
?>