<?php
    class jsonViewer{
        public static function view( $params = array(), $extra ){
            header( 'Content-type: application/json;charset=utf-8');
            echo json_encode( $params );
        }
    }
?>